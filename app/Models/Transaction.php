<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction';
    protected $fillable = ['id_book', 'id_reader', 'amount'];

    /**
     * book function
     * Una transacción pertenece a un libro
     * @return void
     */
    public function book()
    {
        return $this->belongsTo(Book::class, 'id_book', 'id');
    }

    /**
     * reader function
     * Una transacción es realiada por un lector (pertenece a al lector)
     * @return void
     */
    public function reader()
    {
        return $this->belongsTo(Reader::class, 'id_reader', 'id');
    }

    /**
     * donation function
     * Una transacción contiene una donación (pertenece a una donación)
     * @return void
     */
    public function donation()
    {
        return $this->belongsTo(Donation::class, 'id_transaction', 'id');
    }
}