<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $table = 'author';
    protected $fillable = ['name'];

    /**
     * book function
     * Un autor tiene muchos libros
     * @return void
     */
    public function book()
    {
        return $this->hasMany(Book::class, 'id_author', 'id');
    }

    /**
     * notification function
     * Un autor recibe muchas notificaciones
     * @return void
     */
    public function notification()
    {
        return $this->hasMany(Notification::class, 'id_author', 'id');
    }
}