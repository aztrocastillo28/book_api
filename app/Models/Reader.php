<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reader extends Model
{
    protected $table = 'reader';
    protected $fillable = ['name', 'balance'];

    /**
     * transaction function
     * Un lector tiene muchas transacciones
     * @return void
     */
    public function transaction()
    {
        return $this->hasMany(Transaction::class, 'id_reader', 'id');
    }
}