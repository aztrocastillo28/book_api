<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Transaction;

class TransactionController extends Controller
{
    /**
    * @api {get} /transactions/get/:id Descargar .CSV de una transacción.
    * @apiName GetTransaction
    * @apiGroup Transactions
    *
    */
    public function books_report($id_author)
    {
        try {
            $fileName = 'booksReport.csv';
            $data = array();

            $books = Book::where('id_author', $id_author)->with(['discount_promotion', 'help_promotion', 'transaction'])->get();

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            foreach ($books as $book) {
                foreach ($book->transaction as $i => $transaction) {
                    $data[] = array(
                        'id transaction' => $transaction->id,
                        'id book' => $book->id,
                        'name book' => $book->name,
                        'amount' => $transaction->amount,
                        'date' => $transaction->created_at,
                        'reader' => $transaction->reader->name
                    );
                }
            }

            $columns = array(
                'id transaction',
                'id book',
                'name book',
                'amount',
                'date',
                'reader'
            );

            $callback = function () use ($data, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);
                foreach ($data as $item) {
                    fputcsv($file, $item);
                }
                fclose($file);
            };
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'Ocurrió un error al generar reporte de ventas.',
                'content' => $th->getMessage()
            ], 500);
        }

        return response()->stream($callback, 200, $headers);
    }
}