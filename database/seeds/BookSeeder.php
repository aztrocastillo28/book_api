<?php

use Illuminate\Database\Seeder;
use App\Models\Book;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $book = Book::create([
            'id_author' => 1,
            'name' => 'Rojo y Negro',
            'price' => 50.20
        ]);

        $book = Book::create([
            'id_author' => 2,
            'name' => 'El viejo y el mar',
            'price' => 60.30
        ]);

        $book = Book::create([
            'id_author' => 3,
            'name' => 'Tres tristes tigres',
            'price' => 50.00
        ]);

        $book = Book::create([
            'id_author' => 4,
            'name' => 'La isla del tesoro',
            'price' => 30.50
        ]);

        $book = Book::create([
            'id_author' => 5,
            'name' => 'Poesias',
            'price' => 35.00
        ]);

        $book = Book::create([
            'id_author' => 1,
            'name' => 'Canto general',
            'price' => 70.50
        ]);

        $book = Book::create([
            'id_author' => 2,
            'name' => 'Nora o una casa de muñecas',
            'price' => 40.00
        ]);

        $book = Book::create([
            'id_author' => 3,
            'name' => 'La Montaña Mágica',
            'price' => 35.00
        ]);

        $book = Book::create([
            'id_author' => 3,
            'name' => 'Confesiones de una Máscara',
            'price' => 65.00
        ]);

        $book = Book::create([
            'id_author' => 4,
            'name' => 'Guerra Y Paz II',
            'price' => 45.00
        ]);
    }
}