<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Author;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $arrays = range(0, 4);

        foreach ($arrays as $value) {
            Author::create([
                'name' => $faker->firstName() . ' ' . $faker->lastName()
            ]);
        }
    }
}