<?php

use Illuminate\Database\Seeder;
use App\Models\HelpPromotion;

class HelpPromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $help_promotion = HelpPromotion::create([
            'id_book' => 7,
            'start_date' => '2021-03-23',
            'total_quantity' => 200
        ]);

        $help_promotion = HelpPromotion::create([
            'id_book' => 9,
            'start_date' => '2021-03-24',
            'total_quantity' => 300
        ]);
    }
}